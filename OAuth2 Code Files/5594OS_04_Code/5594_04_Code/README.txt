Installation and running the app

    1. Install the newest OpenJDK and Maven 3
    2. Compile the project by running "mvn clean install" in the console, when inside the project
    3. Run the project by executing "mvn tomcat7:run" after step 2) finishes
    4. Open browser and point to http://localhost:8090 and that's it!

The main code example in the book, is located in 
	src\main\webapp\WEB-INF\pages\home.jsp
	src\main\java\example\authorization\MainController.java

	
